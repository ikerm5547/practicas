/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotizacion;

/**
 *
 * @author Iker Martinez
 */
public class Cotizacio{
    private int numCotizacion;
    private String descripcion;
    private float precio;
    private float pagoInicial;
    private int plazo;
    
    public Cotizacio(){
        this.numCotizacion=0123;
        this.descripcion = "Yaris SX";
        this.precio=220000;
        this.pagoInicial=0.25f;
        this.plazo=36;
    }

    /**
     *
     */
    public Cotizacio (int numCotizacion, String descripcion, float precio, float porcentajeInicial, int plazo){
        this.numCotizacion=numCotizacion;
        this.descripcion=descripcion;
        this.precio=precio;
        this.pagoInicial=pagoInicial;
        this.plazo=plazo;
    }
    public Cotizacio(Cotizacio otro){
        this.numCotizacion=otro.numCotizacion;
        this.descripcion=otro.descripcion;
        this.precio=otro.precio;
        this.pagoInicial=otro.pagoInicial;
        this.plazo=otro.plazo;
    }

    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPagoInicial() {
        return pagoInicial;
    }

    public void setPagoInicial(float pagoInicial) {
        this.pagoInicial = pagoInicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

  
    public float calcularPagoInicial() {
    float inicial = this.precio * this.pagoInicial;
    return inicial;
}

public float calcularFin() {
    float inicial = calcularPagoInicial();
    float fin = this.precio - inicial;
    return fin;
}

public float pagoMensual() {
   float mensual = 0;
   float fin = calcularFin();
   mensual = fin / this.plazo;
   return mensual;
    }
}
